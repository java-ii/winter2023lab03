import java.util.Scanner;
public class ApplianceStore{ 
  
  public static void main(String[] args){
    
    Scanner reader = new Scanner(System.in);
    RiceCooker[] riceCookers = new RiceCooker[4];
    System.out.println();
    System.out.println("Welcome to the ApplianceStore Simulator!");
    System.out.println();
    System.out.println("Set the parameters to start the simulation!");
    System.out.println(); 
    for(int i=0; i<riceCookers.length; i++){
      riceCookers[i] = new RiceCooker();
      System.out.println();
      System.out.print("RiceCooker name: ");;
      riceCookers[i].name = reader.next();
      System.out.print("Food type: ");
      riceCookers[i].typeFood = reader.next();
      System.out.print("Amount of water(c): "); 
      riceCookers[i].amountWater = reader.nextInt();
      System.out.print("Time(min): ");
      riceCookers[i].timer = reader.nextInt();
      System.out.print("Quantity(g): "); 
      riceCookers[i].quantity = reader.nextInt(); 
      System.out.print("Temperature(c): ");  
      riceCookers[i].temperature = reader.nextInt();
    }
    System.out.println();
    System.out.println("This is appliance number 4 parameters");
    System.out.println();
    System.out.println("1: " + riceCookers[3].name);
    System.out.println("2: " + riceCookers[3].typeFood);
    System.out.println("3: " + riceCookers[3].quantity);
    System.out.println("4: " + riceCookers[3].timer);
    System.out.println("5: " + riceCookers[3].temperature); 
    System.out.println();
    riceCookers[0].setUp();
    System.out.println();
    riceCookers[0].cookingTime(); 
    System.out.println(); 
    System.out.println("Thank you for participating in ApplianceStore Simulator!"); 
    System.out.println("Until next time!");
  }
}