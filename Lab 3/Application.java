public class Application{
	public static void main(String[] args){
		
	Student borris = new Student(); 
	System.out.println(borris.name = "Borris");
	System.out.println(borris.height = 175);
	System.out.println(borris.hairtype = "Straight");
	System.out.println(borris.eyeColor = "Green");
	System.out.println(borris.favFood = "Pudding");
	System.out.println(borris.favDrink = "Bepsi");
	System.out.println(borris.favAnimal = "Cheetahs");
	System.out.println(); 
	Student karen = new Student(); 
	System.out.println(karen.name = "Karen");
	System.out.println(karen.height = 162);
	System.out.println(karen.hairtype = "Curly");
	System.out.println(karen.eyeColor = "Blue");
	System.out.println(karen.favFood = "Justice");
	System.out.println(karen.favDrink = "Tears");
	System.out.println(karen.favAnimal = "Hyena");
	System.out.println();
	borris.bioInfo();
	System.out.println(); 
	karen.bioInfo();
	System.out.println();
	Student[] section3 = new Student[3]; 
	section3[0] = borris; 
	section3[1] = karen; 
	System.out.println(section3[0].name);
	section3[2] = new Student(); 
	section3[2].name = "Jarry";
	section3[2].height = 157; 
	section3[2].hairtype = "Wavy"; 
	section3[2].eyeColor = "Brown"; 
	section3[2].favFood = "Porridge";
	section3[2].favAnimal = "Dogs";
	section3[2].favDrink = "Chocomilk"; 
	System.out.println(section3[2].name);
	System.out.println();
	section3[2].bioInfo();
	}	
} 

