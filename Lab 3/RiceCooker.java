import java.util.Random;
import java.util.Scanner;
public class RiceCooker{ 
  
  String name; //the series of the cooker
  String typeFood; //fish, rice, chicken, etc.
  int amountWater; //in units of cups
  int timer; //in units of minutes
  int quantity; //in units of grams
  int temperature; //in units of celsius 
  
  public void setUp(){ //sets the parameters time temperature
    System.out.println(name + " is ready to be used!"); 
    System.out.println("Today we are going to be cooking " + typeFood); 
    System.out.println("Preping the Ingredients! Chop! Chop!"); 
    System.out.println("Putting in " + amountWater + " cups of water into " + name + "!"); 
    System.out.println("Putting in " + quantity + " grams of " + typeFood + " and the ingredients into " + name + "!"); 
    System.out.println("Setting the temperature to " + temperature + " Celsius!");
    System.out.println("Setting the time to " + timer + " minutes!");   
  }
  public void cookingTime(){ // sets the parameters for type, amount and quantity
    Scanner reader = new Scanner(System.in);
    System.out.println("Say GET COOKING to start the process!"); 
    System.out.print("Voice: ");
    String voice = reader.next();
    System.out.println("Sweet muffins! Lets get Cooking!");
    Random coinFlip = new Random();      
    boolean heads = true;
    for(int timeLapse = 5; timer>=0;){ 
      boolean flippedCoin = coinFlip.nextBoolean();
      if(flippedCoin != heads){
        System.out.println();
        System.out.println("BEEP! BOOP! BEEP! BOOP!"); 
        System.out.println("COOKING TIME REMAINING: " + timer);
        timer = timer - timeLapse;
      }
      else if(flippedCoin = heads){ 
        System.out.println();
        System.out.println("COOKING! COOKING! COOKING!"); 
        System.out.println("COOKING TIME REMAINING: " + timer);
        timer = timer - timeLapse;
      }
    }   
  }  
} 